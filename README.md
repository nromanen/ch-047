For installing **SERVER PART** on your PC for the first time you need to run 
```
#!shell
cd /server
npm install
```
inside **server** directory. This will install all needed dependencies. Also you should rename the file 
```
#!shell

server/config/default.json.example
```
 to 
```
#!shell

server/config/default.json
```
 and set your own DB settings (Postgres Db preferable) like host name, db name, username and password.
We have a centralized Postgres Db at **heroku** which will simplify our interaction and will remove the burden of installing Postgres server on your own PC.

The settings of centralized Postgres Db:


```
#!shell

    "host": "ec2-54-225-230-243.compute-1.amazonaws.com",
    "port": 5432,
    "name": "d7ef1cmu1jvcdm",
    "user": "dkojgfqufzzrbi",
    "password": "a4f131bbf8cce19bc87ef5692d11589049ebbe1879148e1da5d7a7ca4428aa14"
```


Also consider that **Heroku** *can rotate Db credentials*, so they can be changed during time. So, if your server won't start (due to db connection error), don't panic -- just ask someone for updated credentials.

I would recommend you to use HeidiSQL like a Postgres db manager. http://www.heidisql.com/download.php

After cloning this project to your local PC, please 
```
#!shell

cd
```
 to 
```
#!shell

/server
```
 and start 
```
#!shell

node server.js
```
 in your shell. In case you'll use node, you should restart node every time you change something in your server source code. For more usability, please use nodemon npm package 
```
#!shell

npm install -g nodemon
```
 and use 
```
#!shell

nodemon server.js  --ignore profanity/*.*
```



Also I would recommend you to use this utility https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop?hl=en (Postman for Chrome browser) for testing your REST API endpoints. 


For installing **CLIENT PART** on your PC for the first time you need to run 
```
#!shell
cd /client
npm install
```
inside **client** directory. This will install all needed dependencies. Also you should rename the file 


```
#!shell

client/config.development.json.example
```
 to 
```
#!shell

client/config.development.json
```
 and set your own API Url.

For starting client part of application you need to run ng serve.

Attention! Server and client part are running separately so you need 2 console windows to run application: one with running server with 
```
#!shell

nodemon server.js
```
 and another running client with 
```
#!shell

ng serve
```



PS Good software for using Git GUI and merging http://www.syntevo.com/smartgit/


Enjoy!