var models = require('../models');


module.exports = function (router) {
    'use strict';


    router.route('/')
        .get(function (req, res, next) {

            models.wl.collections.user.find({
                "username": req.query.username
            })
                .then(function (users) {
                    res.json({
                        data: users.length == 0,
                        code: 200
                    });
                }).catch(function (err) {
                res.json({data: err.toString(), code: 500});
            });


        })


};

