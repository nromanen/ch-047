var models = require('../models');
var jwt = require('jsonwebtoken');
var Config = require('config');

module.exports = function (router) {
    'use strict';

    router.route('/')
        .post(function (req, res, next) {

            models.wl.collections.user.findOne({
                "username": "admin"
            }).then(function (user) {
                if (typeof user !== "undefined") {
                    authenticate(req, res);
                }
                else {
                    models.wl.collections.user.create({
                        "username": "admin",
                        "password": models.wl.collections.user.hashPassword("admin"),
                        "roles": ["admin"]
                    }).then(function(user){
                        authenticate(req, res);
                    }).catch(function(err){
                        return reply({data: err.toString(), code: 500});
                    })
                }
            }).catch(function (err) {
                res.json({data: err.toString(), code: 500});
            });


        });

    function authenticate(req, res){
        models.wl.collections.user.findOne({
            username: req.body.username
        })
            .then(function (user) {
                if (user){
                    if (user.password == models.wl.collections.user.hashPassword(req.body.password)){
                        var jwtSign = jwt.sign({iat: Date.now(), exp: parseInt(Date.now()+parseInt(Config.get("auth.jwtExpTime"))), sub: user.id, username: user.username, roles: user.roles}, Config.get("auth.jwtPrivateKey"));

                        res.json({data: "Logged in", token: jwtSign, code: 200});
                    }
                    else {
                        res.json({data: "Wrong password", code: 500});

                    }
                }
                else {
                    res.json({data: "User not found", code: 404});
                }

            }).catch(function (err) {
                res.json({data: err.toString(), code: 500});
        });
    }
};

