'use strict';

var Waterline = require('waterline');

var Inhabitant = {
    identity: 'inhabitant',
    connection: 'default',

    attributes: {
        surname: {
            type: 'string'
        },
        name: {
            type: 'string'
        },
        patronymic: {
            type: 'string'
        },
        appartment: {
            type: 'integer'
        },
        photo: {
            type: 'string'
        },
        phoneNumber: {
            type: 'string'
        },

        user: {
            model: 'user'
        },
        building: {
            model: 'building'
        },
        comments: {
            collection: 'comment',
            via: 'inhabitant'
        },
        calculations: {
            collection: 'calculation',
            via: 'inhabitant'
        }
    }
};


module.exports = Waterline.Collection.extend(Inhabitant);