'use strict';

var Waterline = require('waterline');
var crypto = require('crypto');
var Config = require('config');

var User = {
    identity: 'user',
    connection: 'default',

    attributes: {
        username: {
            type: 'string'
        },
        password: {
            type: 'string'
        },
        passwordRestoreToken: {
            type: 'string'
        },
        email: {
            type: 'string'
        },

        roles: {
            collection: 'role',
            via: 'users'
        },
        notifications: {
            collection: 'notification',
            via: 'users'
        },
        inhabitants: {
            collection: 'inhabitant',
            via: 'user'
        },
        admins: {
            collection: 'admin',
            via: 'user'
        }
    },

    hashPassword: function (password) {
        var hashedPassword = crypto.createHmac(Config.get('auth.algorithmHash'), Config.get("auth.passwordHash")).update(password).digest('hex');
        return hashedPassword;
    }
};


module.exports = Waterline.Collection.extend(User);