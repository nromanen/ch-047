'use strict';

var Waterline = require('waterline');

var Gallery = {
    identity: 'gallery',
    connection: 'default',

    attributes: {
        photoUrl: {
            type: 'string'
        },
        description: {
            type: 'string'
        },
        blog: {
            model: 'blog'
        }
    }
};


module.exports = Waterline.Collection.extend(Gallery);