'use strict';

var Waterline = require('waterline');

var Role = {
    identity: 'role',
    connection: 'default',

    attributes: {
        name: {
            type: 'string'
        },
        description: {
            type: 'string'
        },
        users: {
            collection: 'user',
            via: 'roles'
        }
    }
};


module.exports = Waterline.Collection.extend(Role);