'use strict';

var Waterline = require('waterline');

var Calculation = {
    identity: 'calculation',
    connection: 'default',

    attributes: {
        date: {
            type: 'date'
        },
        toPayAmmount: {
            type: 'float'
        },
        payedAmmount: {
            type: 'float'
        },

        calculationType: {
            model: 'calculationtype'
        },
        inhabitant: {
            model: 'inhabitant'
        }
    }
};


module.exports = Waterline.Collection.extend(Calculation);