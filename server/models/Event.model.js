'use strict';

var Waterline = require('waterline');

var Event = {
    identity: 'event',
    connection: 'default',

    attributes: {
        title: {
            type: 'string'
        },
        description: {
            type: 'string'
        },
        from: {
            type: 'datetime'
        },
        to: {
            type: 'datetime'
        },

        blogs: {
            collection: 'blog',
            via: 'event'
        }
    }
};


module.exports = Waterline.Collection.extend(Event);