'use strict';

var Waterline = require('waterline');

var Blog = {
    identity: 'blog',
    connection: 'default',

    attributes: {
        title: {
            type: 'string'
        },
        text: {
            type: 'text'
        },
        photo: {
            type: 'string'
        },
        isActive: {
            type: 'boolean'
        },
        publicatedFrom: {
            type: 'datetime'
        },
        isProtocol: {
            type: 'boolean'
        },
        fileUrl: {
            type: 'string'
        },

        admin: {
            model: 'admin'
        },
        event: {
            model: 'event'
        },
        comments: {
            collection: 'comment',
            via: 'blog'
        },
        galleries: {
            collection: 'gallery',
            via: 'blog'
        }
    }
};


module.exports = Waterline.Collection.extend(Blog);