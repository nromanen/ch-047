'use strict';

var Waterline = require('waterline');

var Request = {
    identity: 'request',
    connection: 'default',

    attributes: {
        text: {
            type: 'text'
        },
        isDone: {
            type: 'boolean'
        },
        createdAt: {
            type: 'datetime'
        },

        inhabitant: {
            model: 'inhabitant'
        },
        requestType: {
            model: 'requesttype'
        }
    }
};


module.exports = Waterline.Collection.extend(Request);