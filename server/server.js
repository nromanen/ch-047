var express = require('express');
var app = express();
var bodyParser = require("body-parser");
var path = require('path');
var cors = require('cors');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(cors());

var port = process.env.PORT || 9009;

var router = express.Router();

router.get('/', function (req, res) {
    res.json({data: "Server is up"});
});

var models = require('./models');
require('./controllers/index')(app);

app.use(express.static(path.join(__dirname, 'public')));

models.wl.initialize(models.config, function(err, models) {
    if (err) {
        return console.log(err);
    }



    app.listen(port);
    console.log("app starts on port " + port);

});


